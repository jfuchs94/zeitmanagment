package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class AenderungenWerdenErstWirksam extends JDialog {
    public AenderungenWerdenErstWirksam() {
        getContentPane().setBackground(Color.WHITE);
        getContentPane().setLayout(null);

        JButton btnNewButton = new JButton("OK");
        btnNewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnNewButton.setBounds(306, 95, 130, 25);
        getContentPane().add(btnNewButton);

        JTextPane txtpnHinweisDienderungen = new JTextPane();
        txtpnHinweisDienderungen.setFont(new Font("Dialog", Font.BOLD, 14));
        txtpnHinweisDienderungen
                .setText("Hinweis:\n\nDie Änderungen werden erst nach dem Neustart des Programs wirksam!");
        txtpnHinweisDienderungen.setBounds(12, 12, 424, 71);
        getContentPane().add(txtpnHinweisDienderungen);
        setBackground(Color.WHITE);
    }

    private final JPanel contentPanel = new JPanel();

    /**
     * Launch the application.
     */

    /**
     * Create the dialog.
     */
    public void start() {

        AenderungenWerdenErstWirksam dialog = new AenderungenWerdenErstWirksam();
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setVisible(true);

        dialog.setTitle("Hinweis");
        dialog.getContentPane().setBackground(Color.WHITE);
        dialog.setBackground(Color.WHITE);
        dialog.setBounds(100, 150, 450, 200);
        dialog.getContentPane().setLayout(new BorderLayout());
        contentPanel.setBackground(Color.WHITE);
        contentPanel.setLayout(new FlowLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        {
            JTextPane txtpnnderungenWerdenErst = new JTextPane();
            txtpnnderungenWerdenErst.setFont(new Font("DejaVu Serif", Font.BOLD, 16));
            txtpnnderungenWerdenErst.setText("Änderungen werden erst nach einem \nNeustart des Programmes wirksam!");
            contentPanel.add(txtpnnderungenWerdenErst);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setBackground(Color.WHITE);
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                    }
                });
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
        }
    }

}
