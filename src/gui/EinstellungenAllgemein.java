package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import services.DatabaseService;

public class EinstellungenAllgemein {
    private JTextField tfArzt;

    private JTextField tfStrasse;

    private JTextField tfOrt;

    private JTextField tfPlz;

    private JTextField tfRegion;

    private JTextField tfPraxisName;

    /**
     * @wbp.parser.entryPoint
     */
    public void start() {
        JFrame frmEinstellungenAllgemein = new JFrame();
        frmEinstellungenAllgemein.setTitle("Allgemeine Einstellungen");
        frmEinstellungenAllgemein.setVisible(true);
        frmEinstellungenAllgemein.setResizable(false);
        frmEinstellungenAllgemein.getContentPane().setBackground(Color.WHITE);
        frmEinstellungenAllgemein.setMinimumSize(new Dimension(700, 400));
        frmEinstellungenAllgemein.getContentPane().setSize(new Dimension(700, 400));
        frmEinstellungenAllgemein.getContentPane().setPreferredSize(new Dimension(700, 400));
        frmEinstellungenAllgemein.getContentPane().setBounds(new Rectangle(0, 0, 700, 400));
        frmEinstellungenAllgemein.getContentPane().setLayout(null);

        JLabel lblAllgemeineEinstellungen = new JLabel("Allgemeine Einstellungen");
        lblAllgemeineEinstellungen.setHorizontalAlignment(SwingConstants.CENTER);
        lblAllgemeineEinstellungen.setFont(new Font("DejaVu Serif", Font.BOLD, 16));
        lblAllgemeineEinstellungen.setBounds(12, 12, 674, 25);
        frmEinstellungenAllgemein.getContentPane().add(lblAllgemeineEinstellungen);

        JLabel lblNewArzt = new JLabel("Name des neuen Arztes");
        lblNewArzt.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        lblNewArzt.setBounds(59, 67, 254, 15);
        frmEinstellungenAllgemein.getContentPane().add(lblNewArzt);

        tfArzt = new JTextField();
        tfArzt.setBounds(59, 94, 254, 25);
        frmEinstellungenAllgemein.getContentPane().add(tfArzt);
        tfArzt.setColumns(10);

        JLabel lblStrae = new JLabel("Straße");
        lblStrae.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        lblStrae.setBounds(59, 149, 254, 15);
        frmEinstellungenAllgemein.getContentPane().add(lblStrae);

        JLabel lblOrt = new JLabel("Ort");
        lblOrt.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        lblOrt.setBounds(381, 149, 254, 15);
        frmEinstellungenAllgemein.getContentPane().add(lblOrt);

        JLabel lblPostleitzahl = new JLabel("Postleitzahl");
        lblPostleitzahl.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        lblPostleitzahl.setBounds(59, 248, 254, 15);
        frmEinstellungenAllgemein.getContentPane().add(lblPostleitzahl);

        JLabel lblBundesland = new JLabel("Bundesland");
        lblBundesland.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        lblBundesland.setBounds(381, 248, 254, 15);
        frmEinstellungenAllgemein.getContentPane().add(lblBundesland);

        JButton btnSave = new JButton("Speichern");
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String namePraxis = tfPraxisName.getText();
                String region = tfRegion.getText();
                String strasse = tfStrasse.getText();
                String plz = tfPlz.getText();
                String ort = tfOrt.getText();
                String nameArzt = tfArzt.getText();
                DatabaseService databaseService = new DatabaseService();
                databaseService.changePraxisDetails(namePraxis, region, strasse, plz, ort, nameArzt);

                tfPraxisName.setText("");
                tfRegion.setText("");
                tfStrasse.setText("");
                tfPlz.setText("");
                tfOrt.setText("");
                tfArzt.setText("");

                AenderungenWerdenErstWirksam wirksam = new AenderungenWerdenErstWirksam();
                wirksam.start();
            }
        });
        btnSave.setBounds(196, 334, 117, 25);
        frmEinstellungenAllgemein.getContentPane().add(btnSave);

        JButton btnClose = new JButton("Abbrechen");
        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frmEinstellungenAllgemein.dispose();
            }
        });
        btnClose.setBounds(381, 334, 117, 25);
        frmEinstellungenAllgemein.getContentPane().add(btnClose);

        tfStrasse = new JTextField();
        tfStrasse.setColumns(10);
        tfStrasse.setBounds(59, 176, 254, 25);
        frmEinstellungenAllgemein.getContentPane().add(tfStrasse);

        tfOrt = new JTextField();
        tfOrt.setColumns(10);
        tfOrt.setBounds(381, 176, 254, 25);
        frmEinstellungenAllgemein.getContentPane().add(tfOrt);

        tfPlz = new JTextField();
        tfPlz.setColumns(10);
        tfPlz.setBounds(59, 275, 254, 25);
        frmEinstellungenAllgemein.getContentPane().add(tfPlz);

        tfRegion = new JTextField();
        tfRegion.setColumns(10);
        tfRegion.setBounds(381, 278, 254, 25);
        frmEinstellungenAllgemein.getContentPane().add(tfRegion);

        tfPraxisName = new JTextField();
        tfPraxisName.setColumns(10);
        tfPraxisName.setBounds(381, 94, 254, 25);
        frmEinstellungenAllgemein.getContentPane().add(tfPraxisName);

        JLabel lblNameDerPraxis = new JLabel("Name der Praxis");
        lblNameDerPraxis.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        lblNameDerPraxis.setBounds(381, 67, 254, 15);
        frmEinstellungenAllgemein.getContentPane().add(lblNameDerPraxis);

    }
}
