package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import services.DatabaseService;

public class Erinnerung {

    /**
     * @wbp.parser.entryPoint
     */
    public void start() {
        JFrame frmErinnerung = new JFrame();
        frmErinnerung.setTitle("Erinnerung");
        frmErinnerung.setVisible(true);
        frmErinnerung.setResizable(false);
        frmErinnerung.getContentPane().setBackground(Color.WHITE);
        frmErinnerung.setMinimumSize(new Dimension(700, 200));
        frmErinnerung.getContentPane().setSize(new Dimension(700, 400));
        frmErinnerung.getContentPane().setPreferredSize(new Dimension(700, 400));
        frmErinnerung.getContentPane().setBounds(new Rectangle(0, 0, 700, 400));
        frmErinnerung.getContentPane().setLayout(null);

        JLabel lblNewLabel = new JLabel("Geben Sie an wieviel Tage vorher Sie erinnert werden wollen");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("DejaVu Serif", Font.BOLD, 16));
        lblNewLabel.setBounds(12, 12, 674, 21);
        frmErinnerung.getContentPane().add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel("Intervall:");
        lblNewLabel_1.setBounds(12, 118, 674, 15);
        frmErinnerung.getContentPane().add(lblNewLabel_1);

        JSlider slider = new JSlider();
        slider.setPaintTicks(true);
        slider.setBackground(Color.WHITE);
        slider.setForeground(Color.BLACK);
        slider.setSnapToTicks(true);
        slider.setMinorTickSpacing(5);
        slider.setMajorTickSpacing(5);
        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                lblNewLabel_1.setText("Intervall:\t" + slider.getValue());
            }
        });
        slider.setMaximum(30);
        slider.setBounds(12, 70, 674, 31);
        frmErinnerung.getContentPane().add(slider);

        JButton btnNewButton = new JButton("OK");
        btnNewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DatabaseService service = new DatabaseService();
                service.setIntervallErinnerung(slider.getValue());
            }
        });
        btnNewButton.setBounds(196, 134, 117, 25);
        frmErinnerung.getContentPane().add(btnNewButton);

        JButton btnNewButton_1 = new JButton("Schließen");
        btnNewButton_1.setBounds(362, 134, 117, 25);
        frmErinnerung.getContentPane().add(btnNewButton_1);
    }
}
