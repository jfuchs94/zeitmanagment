package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import services.DatabaseService;

public class Table {

    static final long ONE_HOUR = 60 * 60 * 1000L;

    private JTable tableSql;

    private DatabaseService databaseService;

    /**
     * @wbp.parser.entryPoint
     */
    public void start() {
        final JPanel contentPane;

        JFrame frame = new JFrame();
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setMinimumSize(new Dimension(1000, 700));
        frame.setResizable(false);
        frame.getContentPane().setSize(new Dimension(1000, 700));
        frame.getContentPane().setPreferredSize(new Dimension(1000, 700));
        frame.getContentPane().setBounds(new Rectangle(0, 0, 1000, 700));
        frame.setBounds(new Rectangle(0, 24, 1000, 700));
        databaseService = new DatabaseService();
        frame.setTitle("Übersicht");
        frame.setSize(new Dimension(1000, 700));
        frame.setPreferredSize(new Dimension(1000, 700));
        frame.setMaximumSize(new Dimension(1280, 720));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 720, 576);

        final JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        final JMenu mnNewMenu = new JMenu("Program");
        menuBar.add(mnNewMenu);

        final JMenuItem mntmNewMenuItem = new JMenuItem("Neuer Patient");

        contentPane = new JPanel();
        contentPane.setMinimumSize(new Dimension(1000, 700));
        contentPane.setBackground(Color.WHITE);
        contentPane.setMaximumSize(new Dimension(1280, 720));
        contentPane.setLayout(null);

        final JLabel praxisName = new JLabel("");
        praxisName.setFont(new Font("DejaVu Serif", Font.BOLD, 18));
        praxisName.setBounds(12, 12, 618, 22);
        praxisName.setText(databaseService.getPraxisName());
        contentPane.add(praxisName);

        final JLabel arztName = new JLabel((String) null);
        arztName.setFont(new Font("DejaVu Serif", Font.BOLD, 18));
        arztName.setBounds(12, 34, 618, 22);
        arztName.setText(databaseService.getPraxisArzt());
        contentPane.add(arztName);

        tableSql = new JTable();
        tableSql.setFillsViewportHeight(true);
        tableSql.setCellSelectionEnabled(true);
        tableSql.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableSql.setColumnSelectionAllowed(true);
        tableSql.setEnabled(false);
        tableSql.setRowHeight(25);
        tableSql.setDragEnabled(true);
        tableSql.setAutoCreateRowSorter(true);
        tableSql.setFont(new Font("DejaVu Serif", Font.PLAIN, 14));

        RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableSql.getModel());
        tableSql.setRowSorter(sorter);

        tableSql = databaseService.getPatientForTable(tableSql);
        frame.getContentPane().setLayout(null);

        JScrollPane scrollPane = new JScrollPane(tableSql);
        scrollPane.setSize(new Dimension(900, 400));
        scrollPane.setBounds(12, 231, 982, 404);
        contentPane.add(scrollPane);
        JScrollPane scrollPane_1 = new JScrollPane(contentPane);

        JLabel lblLizensiertFr = new JLabel("Lizensiert für:");
        lblLizensiertFr.setFont(new Font("DejaVu Serif", Font.BOLD, 16));
        lblLizensiertFr.setBounds(12, 68, 503, 22);
        contentPane.add(lblLizensiertFr);

        JTextArea anschrift = new JTextArea();
        anschrift.setEditable(false);
        anschrift.setFont(new Font("DejaVu Serif", Font.ITALIC, 14));
        anschrift.setBounds(12, 90, 387, 64);
        contentPane.add(anschrift);
        anschrift.setText(databaseService.getContactDetails());

        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIconTextGap(2);
        lblNewLabel.setIcon(new ImageIcon(Table.class.getResource("/gui/logo.jpg")));
        lblNewLabel.setBounds(642, 0, 352, 207);
        contentPane.add(lblNewLabel);
        scrollPane_1.setSize(new Dimension(1000, 700));
        scrollPane_1.setBounds(0, 0, 1009, 650);
        frame.getContentPane().add(scrollPane_1);

        mntmNewMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                NewPatient newPatient = new NewPatient();
                newPatient.start();
                frame.dispose();
            }

        });
        mnNewMenu.add(mntmNewMenuItem);

        JMenuItem mntmBeenden = new JMenuItem("Beenden");
        mntmBeenden.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });

        JMenuItem mntmPatientLschen = new JMenuItem("Patient Löschen");
        mntmPatientLschen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Delete delete = new Delete();
                delete.start();
                frame.dispose();
            }
        });

        JMenuItem mntmPatientBearbeiten = new JMenuItem("Patient Bearbeiten");
        mntmPatientBearbeiten.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UpdatePatient update = new UpdatePatient();
                update.start();
            }
        });
        mnNewMenu.add(mntmPatientBearbeiten);
        mnNewMenu.add(mntmPatientLschen);
        mnNewMenu.add(mntmBeenden);

        JMenu mnSortieren = new JMenu("Einstellungen");
        menuBar.add(mnSortieren);

        JMenuItem mntmAllgemein = new JMenuItem("Allgemein");
        mntmAllgemein.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EinstellungenAllgemein allgemein = new EinstellungenAllgemein();
                allgemein.start();
            }
        });
        mnSortieren.add(mntmAllgemein);

        JMenuItem mntmErinnerung = new JMenuItem("Erinnerung");
        mntmErinnerung.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Erinnerung erinnerung = new Erinnerung();
                erinnerung.start();
            }
        });
        mnSortieren.add(mntmErinnerung);

        JMenu mnber = new JMenu("Über");
        menuBar.add(mnber);

        JMenuItem mntmHilfe = new JMenuItem("Hilfe");
        mntmHilfe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Help help = new Help();
                help.start();
            }
        });
        mnber.add(mntmHilfe);
        tableSql = databaseService.getPatientForTable(tableSql);
        tableSql.setBounds(12, 231, 982, 404);
        refreshAction(contentPane, scrollPane);
        frame.setVisible(true);
        databaseService.getTerm();
    }

    private void refreshAction(final JPanel contentPane, JScrollPane scrollPane) {
        contentPane.remove(tableSql);
        contentPane.add(tableSql);
        tableSql.updateUI();
        tableSql.setBounds(12, 231, 982, 404);
        contentPane.repaint();
        scrollPane.repaint();
        scrollPane.updateUI();
        scrollPane.setViewportView(tableSql);
    }
}
