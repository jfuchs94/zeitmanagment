package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;

public class Help {
    /**
     * @wbp.parser.entryPoint
     */
    public void start() {

        JFrame frmOver = new JFrame();
        frmOver.setTitle("Hilfe");
        frmOver.setVisible(true);
        frmOver.setResizable(false);
        frmOver.getContentPane().setBackground(Color.WHITE);
        frmOver.setMinimumSize(new Dimension(700, 400));
        frmOver.getContentPane().setSize(new Dimension(700, 400));
        frmOver.getContentPane().setPreferredSize(new Dimension(700, 400));
        frmOver.getContentPane().setBounds(new Rectangle(0, 0, 700, 400));
        frmOver.getContentPane().setLayout(null);

        JLabel label = new JLabel("");
        label.setIconTextGap(2);
        label.setIcon(new ImageIcon(Help.class.getResource("/gui/logo.jpg")));
        label.setBounds(12, 12, 242, 223);
        frmOver.getContentPane().add(label);

        JTextPane txtpnBeiTechnischenProblemen = new JTextPane();
        txtpnBeiTechnischenProblemen.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        txtpnBeiTechnischenProblemen
                .setText("Bei technischen Problemen oder Wünschen kontaktieren Sie bitte:\n\nRichard Wohlfeld: richard.wohlfeld@hotmail.de\n\n\nBei Fragen an die Entwicklung kontaktieren Sie bitte:\n\nJonathan Fuchs:\njfuchs94@icloud.com");
        txtpnBeiTechnischenProblemen.setBounds(311, 45, 375, 256);
        frmOver.getContentPane().add(txtpnBeiTechnischenProblemen);

        JLabel lblNewLabel = new JLabel("Alle Rechte liegen bei Jonathan Fuchs und Richard Wohlfeld; \u00a9 2015");
        lblNewLabel.setBounds(12, 344, 674, 15);
        frmOver.getContentPane().add(lblNewLabel);

        JButton btnSchlieen = new JButton("Schließen");
        btnSchlieen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frmOver.dispose();
            }
        });
        btnSchlieen.setBounds(569, 339, 117, 25);
        frmOver.getContentPane().add(btnSchlieen);

    }
}
