package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import services.DatabaseService;

public class NewPatient {
    private JTextField tfNamePatient;

    private JTextField tfRegion;

    private JTextField tfArt;

    private JTextField tfTermin;

    private JTextField tfFarbe;

    /**
     * @param scrollPane
     * @param contentPane
     * @wbp.parser.entryPoint
     */
    public void start() {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate now = LocalDate.now();

        DatabaseService databaseService = new DatabaseService();
        Table table = new Table();

        JFrame frmNeuerPatient = new JFrame();
        frmNeuerPatient.setVisible(true);
        frmNeuerPatient.setResizable(false);
        frmNeuerPatient.getContentPane().setBackground(Color.WHITE);
        frmNeuerPatient.setMinimumSize(new Dimension(700, 400));
        frmNeuerPatient.getContentPane().setSize(new Dimension(700, 400));
        frmNeuerPatient.getContentPane().setPreferredSize(new Dimension(700, 400));
        frmNeuerPatient.getContentPane().setBounds(new Rectangle(0, 0, 700, 400));
        frmNeuerPatient.getContentPane().setLayout(null);

        JButton btnPatienAnlegen = new JButton("Patien anlegen");
        btnPatienAnlegen.setBounds(178, 510, 156, 25);
        frmNeuerPatient.getContentPane().add(btnPatienAnlegen);

        JButton btnAbbrechen = new JButton("Abbrechen");
        btnAbbrechen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frmNeuerPatient.dispose();
                table.start();
            }
        });
        btnAbbrechen.setBounds(372, 510, 156, 25);
        frmNeuerPatient.getContentPane().add(btnAbbrechen);

        JLabel lblAnlegenEinesNeuen = new JLabel("Anlegen eines neuen Patienten");
        lblAnlegenEinesNeuen.setHorizontalAlignment(SwingConstants.CENTER);
        lblAnlegenEinesNeuen.setFont(new Font("DejaVu Serif", Font.BOLD, 16));
        lblAnlegenEinesNeuen.setBounds(12, 12, 694, 31);
        frmNeuerPatient.getContentPane().add(lblAnlegenEinesNeuen);

        JLabel lblNewLabel = new JLabel("Name des Patienten");
        lblNewLabel.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblNewLabel.setBounds(63, 88, 244, 31);
        frmNeuerPatient.getContentPane().add(lblNewLabel);

        tfNamePatient = new JTextField();
        tfNamePatient.setBounds(63, 131, 244, 31);
        frmNeuerPatient.getContentPane().add(tfNamePatient);
        tfNamePatient.setColumns(10);

        JLabel lblRegionDerArbeit = new JLabel("Region der Arbeit");
        lblRegionDerArbeit.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblRegionDerArbeit.setBounds(387, 88, 244, 31);
        frmNeuerPatient.getContentPane().add(lblRegionDerArbeit);

        tfRegion = new JTextField();
        tfRegion.setColumns(10);
        tfRegion.setBounds(387, 131, 244, 31);
        frmNeuerPatient.getContentPane().add(tfRegion);

        JLabel lblArtDerArbeit = new JLabel("Art der Arbeit");
        lblArtDerArbeit.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblArtDerArbeit.setBounds(63, 211, 244, 31);
        frmNeuerPatient.getContentPane().add(lblArtDerArbeit);

        JLabel lblTermin = new JLabel("Termin");
        lblTermin.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblTermin.setBounds(387, 211, 244, 31);
        frmNeuerPatient.getContentPane().add(lblTermin);

        tfArt = new JTextField();
        tfArt.setColumns(10);
        tfArt.setBounds(63, 254, 244, 31);
        frmNeuerPatient.getContentPane().add(tfArt);

        tfTermin = new JTextField();
        tfTermin.setText(now.format(dateFormat));
        tfTermin.setColumns(10);
        tfTermin.setBounds(387, 254, 244, 31);
        frmNeuerPatient.getContentPane().add(tfTermin);

        JLabel lblFarbeDesZahns = new JLabel("Farbe des Zahns");
        lblFarbeDesZahns.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblFarbeDesZahns.setBounds(63, 338, 244, 31);
        frmNeuerPatient.getContentPane().add(lblFarbeDesZahns);

        JLabel lblStatusDerArbeit = new JLabel("Status der Arbeit");
        lblStatusDerArbeit.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblStatusDerArbeit.setBounds(387, 338, 244, 31);
        frmNeuerPatient.getContentPane().add(lblStatusDerArbeit);

        tfFarbe = new JTextField();
        tfFarbe.setColumns(10);
        tfFarbe.setBounds(63, 381, 244, 31);
        frmNeuerPatient.getContentPane().add(tfFarbe);

        JCheckBox bxKonst = new JCheckBox("Konstruiert");
        bxKonst.setBackground(Color.WHITE);
        bxKonst.setBounds(387, 377, 132, 25);
        frmNeuerPatient.getContentPane().add(bxKonst);

        JCheckBox bxGesch = new JCheckBox("Geschliffen");
        bxGesch.setBackground(Color.WHITE);
        bxGesch.setBounds(387, 408, 132, 25);
        frmNeuerPatient.getContentPane().add(bxGesch);

        JCheckBox bxGebrannt = new JCheckBox("Gebrannt");
        bxGebrannt.setBackground(Color.WHITE);
        bxGebrannt.setBounds(387, 439, 132, 25);
        frmNeuerPatient.getContentPane().add(bxGebrannt);

        JCheckBox bxFertig = new JCheckBox("Fertiggestellt");
        bxFertig.setBackground(Color.WHITE);
        bxFertig.setBounds(525, 375, 132, 25);
        frmNeuerPatient.getContentPane().add(bxFertig);

        JLabel lblNewLabel_1 = new JLabel("Weiteren Patienten Anlegen?");
        lblNewLabel_1.setVisible(false);
        lblNewLabel_1.setBounds(244, 46, 235, 15);
        frmNeuerPatient.getContentPane().add(lblNewLabel_1);
        frmNeuerPatient.setBounds(new Rectangle(0, 24, 700, 400));
        frmNeuerPatient.setTitle("Neuer Patient");
        frmNeuerPatient.setSize(new Dimension(682, 576));
        frmNeuerPatient.setPreferredSize(new Dimension(700, 400));
        frmNeuerPatient.setMaximumSize(new Dimension(1280, 720));
        frmNeuerPatient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmNeuerPatient.setBounds(100, 100, 720, 576);

        btnPatienAnlegen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = tfNamePatient.getText();
                    String region = tfRegion.getText();
                    String art = tfArt.getText();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    String termin = tfTermin.getText();
                    String farbe = tfFarbe.getText();
                    String status = setStringStatus();
                    Date date = simpleDateFormat.parse(termin);
                    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                    databaseService.insertPatient(name, region, art, sqlDate, farbe, status);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                tfNamePatient.setText("");
                tfArt.setText("");
                tfFarbe.setText("");
                tfRegion.setText("");
                tfTermin.setText(now.format(dateFormat));
                bxFertig.setSelected(false);
                bxGebrannt.setSelected(false);
                bxGesch.setSelected(false);
                bxKonst.setSelected(false);
                lblNewLabel_1.setVisible(true);

            }

            private String setStringStatus() {
                String result = "";
                if (bxGebrannt.isSelected()) {
                    result = "Gebrannt";
                } else if (bxGesch.isSelected()) {
                    result = "Geschliffen";
                } else if (bxKonst.isSelected()) {
                    result = "Konstruiert";
                } else if (bxFertig.isSelected()) {
                    result = "Fertiggestellt";
                }
                return result;
            }
        });
    }
}
