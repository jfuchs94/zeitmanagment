package gui;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import services.DatabaseService;

public class FirstStart extends JFrame {

    private final JPanel contentPane;

    private DatabaseService databaseService;

    private final JTextField praxisNameField;

    private final JTextField regionField;

    private final JTextField strasseField;

    private final JTextField plzField;

    /**
     * Launch the application.
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    final DatabaseService databaseService = new DatabaseService();
                    if (!databaseService.checkInit()) {
                        final FirstStart frame = new FirstStart();
                        frame.setVisible(true);
                    } else {
                        final Table table = new Table();
                        table.start();
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public FirstStart() {
        setBackground(Color.LIGHT_GRAY);
        setTitle("Ersteinrichtung");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 713, 356);
        contentPane = new JPanel();
        contentPane.setBackground(Color.WHITE);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        final JLabel welcomeLabel = new JLabel("Wilkommen bei Ihrem Zeitmanagement-System");
        welcomeLabel.setFont(new Font("Dialog", Font.BOLD, 14));
        welcomeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        welcomeLabel.setBounds(172, 12, 394, 15);
        contentPane.add(welcomeLabel);

        final JLabel initLabel = new JLabel("Initialisierung ihres Produktes");
        initLabel.setBounds(249, 27, 231, 15);
        contentPane.add(initLabel);

        final JLabel praxisNameLabel = new JLabel("Name der Praxis");
        praxisNameLabel.setBounds(12, 88, 138, 15);
        contentPane.add(praxisNameLabel);

        praxisNameField = new JTextField();
        praxisNameField.setBounds(12, 115, 189, 19);
        contentPane.add(praxisNameField);
        praxisNameField.setColumns(10);

        final JLabel lblBundesland = new JLabel("Bundesland");
        lblBundesland.setBounds(249, 88, 124, 15);
        contentPane.add(lblBundesland);

        regionField = new JTextField();
        regionField.setColumns(10);
        regionField.setBounds(249, 115, 189, 19);
        contentPane.add(regionField);

        final JLabel lblStrae = new JLabel("Straße");
        lblStrae.setBounds(486, 88, 124, 15);
        contentPane.add(lblStrae);

        strasseField = new JTextField();
        strasseField.setColumns(10);
        strasseField.setBounds(486, 115, 189, 19);
        contentPane.add(strasseField);

        final JLabel lblPostleitzahl = new JLabel("Postleitzahl");
        lblPostleitzahl.setBounds(12, 163, 138, 15);
        contentPane.add(lblPostleitzahl);

        plzField = new JTextField();
        plzField.setColumns(10);
        plzField.setBounds(12, 190, 189, 19);
        contentPane.add(plzField);

        final JLabel lblOrt = new JLabel("Ort");
        lblOrt.setBounds(249, 163, 138, 15);
        contentPane.add(lblOrt);

        final TextField ortField = new TextField();
        ortField.setBounds(249, 190, 189, 19);
        contentPane.add(ortField);

        final JLabel lblArztName = new JLabel("Name des Arztes");
        lblArztName.setBounds(486, 163, 138, 15);
        contentPane.add(lblArztName);

        final TextField arztField = new TextField();
        arztField.setBounds(486, 190, 189, 19);
        contentPane.add(arztField);

        final Button button = new Button("Weiter");
        button.setBackground(UIManager.getColor("Button.background"));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                databaseService = new DatabaseService();
                databaseService.initTestPatient();
                databaseService.initTestPraxis();
                databaseService.initErrinerung();
                databaseService.writeInPraxis(praxisNameField.getText(), regionField.getText(), strasseField.getText(),
                        plzField.getText(), ortField.getText(), arztField.getText());
                Table tableLayout = new Table();
                tableLayout.start();
                dispose();
            }
        });
        button.setBounds(189, 294, 140, 23);
        contentPane.add(button);

        final Button button_1 = new Button("Beenden");
        button_1.setBackground(UIManager.getColor("Button.background"));
        button_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                dispose();
            }
        });
        button_1.setBounds(378, 294, 140, 23);
        contentPane.add(button_1);

    }
}
