package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import services.DatabaseService;

public class UpdatePatient {
    private JTextField tfNamePatient;

    private JTextField tfRegion;

    private JTextField tfArt;

    private JTextField tfTermin;

    private JTextField tfFarbe;

    /**
     * @param scrollPane
     * @param contentPane
     * @wbp.parser.entryPoint
     */
    public void start() {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate now = LocalDate.now();

        DatabaseService databaseService = new DatabaseService();
        Table table = new Table();

        JFrame frmUpdate = new JFrame();
        frmUpdate.setVisible(true);
        frmUpdate.setResizable(false);
        frmUpdate.getContentPane().setBackground(Color.WHITE);
        frmUpdate.setMinimumSize(new Dimension(700, 400));
        frmUpdate.getContentPane().setSize(new Dimension(700, 400));
        frmUpdate.getContentPane().setPreferredSize(new Dimension(700, 400));
        frmUpdate.getContentPane().setBounds(new Rectangle(0, 0, 700, 400));
        frmUpdate.getContentPane().setLayout(null);

        JButton btnPatienAnlegen = new JButton("Patien speichern");
        btnPatienAnlegen.setBounds(178, 510, 156, 25);
        frmUpdate.getContentPane().add(btnPatienAnlegen);

        JButton btnAbbrechen = new JButton("Abbrechen");
        btnAbbrechen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frmUpdate.dispose();
                table.start();
            }
        });
        btnAbbrechen.setBounds(372, 510, 156, 25);
        frmUpdate.getContentPane().add(btnAbbrechen);

        JLabel lblAnlegenEinesNeuen = new JLabel("Bearbeiten eines neuen Patienten");
        lblAnlegenEinesNeuen.setHorizontalAlignment(SwingConstants.CENTER);
        lblAnlegenEinesNeuen.setFont(new Font("DejaVu Serif", Font.BOLD, 16));
        lblAnlegenEinesNeuen.setBounds(12, 12, 694, 31);
        frmUpdate.getContentPane().add(lblAnlegenEinesNeuen);

        JLabel lblNewLabel = new JLabel("Name des Patienten");
        lblNewLabel.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblNewLabel.setBounds(63, 88, 244, 31);
        frmUpdate.getContentPane().add(lblNewLabel);

        tfNamePatient = new JTextField();
        tfNamePatient.setBounds(63, 131, 244, 31);
        frmUpdate.getContentPane().add(tfNamePatient);
        tfNamePatient.setColumns(10);

        JLabel lblRegionDerArbeit = new JLabel("Region der Arbeit");
        lblRegionDerArbeit.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblRegionDerArbeit.setBounds(387, 88, 244, 31);
        frmUpdate.getContentPane().add(lblRegionDerArbeit);

        tfRegion = new JTextField();
        tfRegion.setColumns(10);
        tfRegion.setBounds(387, 131, 244, 31);
        frmUpdate.getContentPane().add(tfRegion);

        JLabel lblArtDerArbeit = new JLabel("Art der Arbeit");
        lblArtDerArbeit.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblArtDerArbeit.setBounds(63, 211, 244, 31);
        frmUpdate.getContentPane().add(lblArtDerArbeit);

        JLabel lblTermin = new JLabel("Termin");
        lblTermin.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblTermin.setBounds(387, 211, 244, 31);
        frmUpdate.getContentPane().add(lblTermin);

        tfArt = new JTextField();
        tfArt.setColumns(10);
        tfArt.setBounds(63, 254, 244, 31);
        frmUpdate.getContentPane().add(tfArt);

        tfTermin = new JTextField();
        tfTermin.setText(now.format(dateFormat));
        tfTermin.setColumns(10);
        tfTermin.setBounds(387, 254, 244, 31);
        frmUpdate.getContentPane().add(tfTermin);

        JLabel lblFarbeDesZahns = new JLabel("Farbe des Zahns");
        lblFarbeDesZahns.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblFarbeDesZahns.setBounds(63, 338, 244, 31);
        frmUpdate.getContentPane().add(lblFarbeDesZahns);

        JLabel lblStatusDerArbeit = new JLabel("Status der Arbeit");
        lblStatusDerArbeit.setFont(new Font("DejaVu Serif", Font.BOLD | Font.ITALIC, 14));
        lblStatusDerArbeit.setBounds(387, 338, 244, 31);
        frmUpdate.getContentPane().add(lblStatusDerArbeit);

        tfFarbe = new JTextField();
        tfFarbe.setColumns(10);
        tfFarbe.setBounds(63, 381, 244, 31);
        frmUpdate.getContentPane().add(tfFarbe);

        JCheckBox bxKonst = new JCheckBox("Konstruiert");
        bxKonst.setBackground(Color.WHITE);
        bxKonst.setBounds(387, 377, 132, 25);
        frmUpdate.getContentPane().add(bxKonst);

        JCheckBox bxGesch = new JCheckBox("Geschliffen");
        bxGesch.setBackground(Color.WHITE);
        bxGesch.setBounds(387, 408, 132, 25);
        frmUpdate.getContentPane().add(bxGesch);

        JCheckBox bxGebrannt = new JCheckBox("Gebrannt");
        bxGebrannt.setBackground(Color.WHITE);
        bxGebrannt.setBounds(387, 439, 132, 25);
        frmUpdate.getContentPane().add(bxGebrannt);

        JCheckBox bxFertig = new JCheckBox("Fertiggestellt");
        bxFertig.setBackground(Color.WHITE);
        bxFertig.setBounds(525, 375, 132, 25);
        frmUpdate.getContentPane().add(bxFertig);

        JLabel lblNewLabel_1 = new JLabel("Weiteren Patienten Bearbeiten?");
        lblNewLabel_1.setVisible(false);
        lblNewLabel_1.setBounds(244, 46, 235, 15);
        frmUpdate.getContentPane().add(lblNewLabel_1);

        JButton btnSuchen = new JButton("Suchen");
        btnSuchen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HashMap<String, String> patient = databaseService.fillPatientSearchGui(tfNamePatient.getText());
            }
        });
        btnSuchen.setBounds(110, 174, 156, 25);
        frmUpdate.getContentPane().add(btnSuchen);
        frmUpdate.setBounds(new Rectangle(0, 24, 700, 400));
        frmUpdate.setTitle("Neuer Patient");
        frmUpdate.setSize(new Dimension(682, 576));
        frmUpdate.setPreferredSize(new Dimension(700, 400));
        frmUpdate.setMaximumSize(new Dimension(1280, 720));
        frmUpdate.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmUpdate.setBounds(100, 100, 720, 576);

        btnPatienAnlegen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            }

        });
    }

}
