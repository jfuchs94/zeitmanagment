package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import services.DatabaseService;

public class Delete {
    private JTextField textField;

    /**
     * @wbp.parser.entryPoint
     */
    @SuppressWarnings("deprecation")
    public void start() {

        JFrame frmHelp = new JFrame();
        frmHelp.setTitle("Löschen");
        frmHelp.setVisible(true);
        frmHelp.setResizable(false);
        frmHelp.getContentPane().setBackground(Color.WHITE);
        frmHelp.setMinimumSize(new Dimension(700, 300));
        frmHelp.getContentPane().setSize(new Dimension(700, 400));
        frmHelp.getContentPane().setPreferredSize(new Dimension(700, 400));
        frmHelp.getContentPane().setBounds(new Rectangle(0, 0, 700, 400));
        frmHelp.getContentPane().setLayout(null);

        JLabel lblPatientenLschen = new JLabel("Patient/en Löschen");
        lblPatientenLschen.setHorizontalAlignment(SwingConstants.CENTER);
        lblPatientenLschen.setFont(new Font("DejaVu Serif", Font.BOLD, 16));
        lblPatientenLschen.setBounds(12, 12, 674, 15);
        frmHelp.getContentPane().add(lblPatientenLschen);

        JLabel lblNewLabel = new JLabel("Name des zu löschenden Patienten");
        lblNewLabel.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        lblNewLabel.setBounds(12, 59, 305, 15);
        frmHelp.getContentPane().add(lblNewLabel);

        textField = new JTextField();
        textField.setBounds(12, 80, 305, 19);
        frmHelp.getContentPane().add(textField);
        textField.setColumns(10);

        JCheckBox chckbxAlleFertiggestelltenPatienten = new JCheckBox("Alle fertiggestellten Patienten löschen");
        chckbxAlleFertiggestelltenPatienten.setFont(new Font("DejaVu Serif", Font.BOLD, 12));
        chckbxAlleFertiggestelltenPatienten.setBackground(Color.WHITE);
        chckbxAlleFertiggestelltenPatienten.setBounds(12, 123, 305, 28);
        frmHelp.getContentPane().add(chckbxAlleFertiggestelltenPatienten);

        JCheckBox chckbxAllePatientenLschen = new JCheckBox("Alle Patienten löschen");
        chckbxAllePatientenLschen.setFont(new Font("DejaVu Serif", Font.BOLD, 12));
        chckbxAllePatientenLschen.setBackground(Color.WHITE);
        chckbxAllePatientenLschen.setBounds(12, 157, 305, 28);
        frmHelp.getContentPane().add(chckbxAllePatientenLschen);

        JLabel lblNewLabel_1 = new JLabel("Erfolgreich gelöscht!");
        lblNewLabel_1.setVisible(false);
        lblNewLabel_1.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        lblNewLabel_1.setForeground(Color.RED);
        lblNewLabel_1.setBounds(350, 82, 336, 15);

        frmHelp.getContentPane().add(lblNewLabel_1);
        JButton btnWeiter = new JButton("Weiter");
        btnWeiter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String delete = "";
                String select;
                if (chckbxAlleFertiggestelltenPatienten.isSelected()) {
                    delete = "Status = 'fertiggestellt';";
                } else if (chckbxAllePatientenLschen.isSelected()) {
                    delete = ";";
                } else if (textField.getText() != "" && textField.getText() != null) {
                    delete = "name = '" + textField.getText() + "';";
                }
                select = delete;
                DatabaseService databaseService = new DatabaseService();
                databaseService.deletePatient(delete, select);
                lblNewLabel_1.setVisible(true);

            }
        });
        btnWeiter.setBounds(221, 234, 117, 25);
        frmHelp.getContentPane().add(btnWeiter);

        JButton btnNewButton = new JButton("Beenden");
        btnNewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frmHelp.dispose();
                Table table = new Table();
                table.start();
            }
        });
        btnNewButton.setBounds(350, 234, 117, 25);
        frmHelp.getContentPane().add(btnNewButton);

    }
}
