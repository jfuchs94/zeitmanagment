package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import services.DatabaseService;

public class TerminPop {

    /**
     * @wbp.parser.entryPoint
     */
    public void start() {
        JFrame frmTermin = new JFrame();
        frmTermin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frmTermin.setAlwaysOnTop(true);
        frmTermin.setTitle("Termine");
        frmTermin.setVisible(true);
        frmTermin.setResizable(false);
        frmTermin.getContentPane().setBackground(Color.WHITE);
        frmTermin.setMinimumSize(new Dimension(700, 400));
        frmTermin.getContentPane().setSize(new Dimension(700, 400));
        frmTermin.getContentPane().setPreferredSize(new Dimension(700, 400));
        frmTermin.getContentPane().setBounds(new Rectangle(0, 0, 700, 400));
        frmTermin.getContentPane().setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBackground(Color.WHITE);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0, 0, 698, 371);
        frmTermin.getContentPane().add(scrollPane);

        JTextArea textArea = new JTextArea();
        textArea.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
        textArea.setEditable(false);
        scrollPane.setViewportView(textArea);

        DatabaseService service = new DatabaseService();
        textArea.setText(service.getTerm());

        JLabel lblTermine = new JLabel("Termine");
        lblTermine.setBackground(Color.WHITE);
        lblTermine.setFont(new Font("DejaVu Serif", Font.BOLD, 16));
        lblTermine.setHorizontalAlignment(SwingConstants.CENTER);
        scrollPane.setColumnHeaderView(lblTermine);
    }
}
