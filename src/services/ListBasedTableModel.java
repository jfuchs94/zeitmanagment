package services;

import java.util.List;

import javax.swing.table.DefaultTableModel;

public class ListBasedTableModel extends DefaultTableModel {

    List data;

    public ListBasedTableModel(final List data, final Object[] columnNames) {
        super(columnNames, data.size());
        this.data = data;
    }

    @Override
    public Object getValueAt(final int row, final int column) {
        return ((Object[]) data.get(row))[column];
    }
}
