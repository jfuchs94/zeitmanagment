package services;

import gui.TerminPop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DatabaseService {
    static final long ONE_HOUR = 60 * 60 * 1000L;

    private Connection conn;

    private List<Date> termDate = new ArrayList<>();

    private Statement stmt;

    private static boolean isTermStarted = false;

    public boolean checkInit() {
        ResultSet isInit;
        try {
            prepareStatement();
            final String selectFromPatient = "SELECT init FROM PRAXIS;";
            isInit = stmt.executeQuery(selectFromPatient);
            while (isInit.next()) {
                if (isInit.getInt("init") == 1) {
                    return true;
                }
            }
            conn.close();
        } catch (final SQLException e) {
            if (e.getMessage().contains("Ungültiger")) {
                createTablePatient();
            }
        }

        return false;
    }

    public void initTestPatient() {
        try {
            prepareStatement();
            final String selectFromPatient = "SELECT * FROM PATIENT;";
            stmt.execute(selectFromPatient);
            conn.close();
        } catch (final SQLException e) {
            if (e.getMessage().contains("Ungültiger")) {
                createTablePatient();
            }
        }
    }

    public void initTestPraxis() {
        try {
            prepareStatement();
            final String selectFromPraxis = "SELECT * FROM PRAXIS";
            stmt.execute(selectFromPraxis);
            conn.close();
        } catch (final SQLException sql) {
            if (sql.getMessage().contains("Ungültiger")) {
                createTablePraxis();
            } else {
                sql.printStackTrace();
            }
        }

    }

    private void createTablePraxis() {
        try {
            prepareStatement();
            final String createTablePraxis = "CREATE TABLE PRAXIS" + "(" + "id int identity primary key,"
                    + "name varchar(255)," + "region varchar(255)," + "strasse varchar(255)," + "plz int,"
                    + "ort varchar(255)," + "arzt varchar(255)," + "init bit,);";
            stmt.execute(createTablePraxis);
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }

    }

    private void createTablePatient() {
        try {
            prepareStatement();
            final String createTablePatient = "CREATE TABLE PATIENT" + "(" + "id int identity primary key,"
                    + "Name varchar(255)," + "Region varchar(255)," + "Art varchar(255)," + "Termin date,"
                    + "Farbe varchar(255)," + "Status varchar(255)," + ");";
            stmt.execute(createTablePatient);
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public void writeInPraxis(final String name, final String region, final String strasse, final String plz,
            final String ort, final String arzt) {
        try {
            prepareStatement();
            final String insertInPraxis = "INSERT INTO PRAXIS(name, region, strasse, plz, ort, arzt, init)"
                    + "VALUES ('" + name + "','" + region + "','" + strasse + "','" + Integer.parseInt(plz) + "','"
                    + ort + "','" + arzt + "','1');";
            stmt.execute(insertInPraxis);
            conn.close();
        } catch (final SQLException sql) {
            sql.printStackTrace();
        }
    }

    public void changePraxisDetails(final String name, final String region, final String strasse, final String plz,
            final String ort, final String arzt) {
        try {
            prepareStatement();
            final String insertInPraxis = "DELETE FROM PRAXIS WHERE ID='1'; INSERT INTO PRAXIS(name, region, strasse, plz, ort, arzt, init)"
                    + "VALUES ('"
                    + name
                    + "','"
                    + region
                    + "','"
                    + strasse
                    + "','"
                    + Integer.parseInt(plz)
                    + "','"
                    + ort + "','" + arzt + "','1');";
            stmt.execute(insertInPraxis);
            conn.close();
        } catch (final SQLException sql) {
            sql.printStackTrace();
        }
    }

    public String getPraxisName() {
        final ResultSet name;
        String praxis = "";
        String result = "";
        try {
            prepareStatement();
            final String readName = "SELECT name FROM PRAXIS;";
            name = stmt.executeQuery(readName);
            while (name.next()) {
                praxis = name.getString("name");
            }
            result = "Praxis: " + praxis;
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPraxisArzt() {
        final ResultSet arzt;
        String arztName = "";
        String result = "";

        try {
            prepareStatement();
            final String getArztName = "SELECT arzt FROM PRAXIS;";
            arzt = stmt.executeQuery(getArztName);
            while (arzt.next()) {
                arztName = arzt.getString("arzt");
            }
            result = "Arzt: " + arztName;
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getPraxisRegion() {
        final ResultSet region;
        String praxisRegion = "";
        String result = "";

        try {
            prepareStatement();
            final String getRegion = "SELECT region FROM PRAXIS;";
            region = stmt.executeQuery(getRegion);
            while (region.next()) {
                praxisRegion = region.getString("region");
            }
            result = praxisRegion;
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getPraxisPlz() {
        final ResultSet plz;
        String praxisPlz = "";
        String result = "";

        try {
            prepareStatement();
            final String getPlz = "SELECT plz FROM PRAXIS;";
            plz = stmt.executeQuery(getPlz);
            while (plz.next()) {
                praxisPlz = plz.getString("plz");
            }
            result = praxisPlz;
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getPraxisOrt() {
        final ResultSet ort;
        String praxisOrt = "";
        String result = "";

        try {
            prepareStatement();
            final String getOrt = "SELECT ort FROM PRAXIS;";
            ort = stmt.executeQuery(getOrt);
            while (ort.next()) {
                praxisOrt = ort.getString("ort");
            }
            result = praxisOrt;
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getPraxisStrasse() {
        final ResultSet strasse;
        String praxisStrasse = "";
        String result = "";

        try {
            prepareStatement();
            final String getStrasse = "SELECT strasse FROM PRAXIS;";
            strasse = stmt.executeQuery(getStrasse);
            while (strasse.next()) {
                praxisStrasse = strasse.getString("strasse");
            }
            result = praxisStrasse;
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getContactDetails() {
        final String praxisName = getPraxisName();
        final String region = getPraxisRegion();
        final String plz = getPraxisPlz();
        final String ort = getPraxisOrt();
        final String strasse = getPraxisStrasse();
        return praxisName + "\n" + region + "\n" + plz + ", " + ort + "\n" + strasse;
    }

    public JTable getPatientForTable(final JTable tblSqlData) {
        final JTable jTable = tblSqlData;
        try {
            prepareStatement();
            final String getPatient = "Select Name, Region, Art, Termin, Farbe, Status From Patient";
            final ResultSet rs = stmt.executeQuery(getPatient);

            final DefaultTableModel dtm = buildTableModelForm(rs);
            jTable.setModel(dtm);
            jTable.updateUI();
        } catch (final SQLException e) {
            createTablePatient();
        }
        return jTable;
    }

    private DefaultTableModel buildTableModelForm(final ResultSet rs) throws SQLException {
        final ResultSetMetaData rsmd = rs.getMetaData();
        final int clmCnt = rsmd.getColumnCount();

        final Object[] columnNames = new Object[clmCnt];
        for (int i = 1; i <= columnNames.length; i++) {
            columnNames[i - 1] = rsmd.getColumnName(i);
        }

        final List rows = new ArrayList();
        while (rs.next()) {
            final Object[] row = new Object[clmCnt];
            for (int i = 1; i <= clmCnt; i++) {
                row[i - 1] = rs.getString(i);
            }
            rows.add(row);
        }
        return new ListBasedTableModel(rows, columnNames);
    }

    public void insertPatient(String name, String region, String art, Date termin, String farbe, String status)
            throws SQLException {
        prepareStatement();
        final String patienName = name;
        final String regionPatient = region;
        final String artWork = art;
        final Date term = termin;
        final String color = farbe;
        final String state = status;
        final String insertPatient = "INSERT INTO PATIENT(Name, Region, Art, Termin, Farbe, Status) " + "VALUES('"
                + patienName + "','" + regionPatient + "','" + artWork + "','" + term + "','" + color + "','" + state
                + "')";
        stmt.execute(insertPatient);
    }

    public Connection connect() throws SQLException {
        final String USERNAME = "jfs_77740_avviso_web";
        final String PASSWORD = "avviso";

        final String url = "jdbc:jtds:sqlserver://DBDEVEL:1433;instance=MS2008R2;sendStringParametersAsUnicode=false";
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
        }
        final Connection conn = DriverManager.getConnection(url, USERNAME, PASSWORD);
        return conn;
    }

    private void prepareStatement() throws SQLException {
        conn = connect();
        stmt = conn.createStatement();
    }

    public void initErrinerung() {
        try {
            prepareStatement();
            final String createTablePraxis = "CREATE TABLE ERINNERUNG" + "("
                    + "id int identity primary key, intervall int);";
            stmt.execute(createTablePraxis);
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    public void setIntervallErinnerung(int intervall) {
        final int erinnerung = intervall;
        try {
            prepareStatement();
            final String insertPatient = "INSERT INTO ERINNERUNG(intervall) " + "VALUES('" + erinnerung + "')";
            stmt.execute(insertPatient);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getIntervall() {
        final ResultSet rs;
        int intervall = 0;
        int result = 0;
        try {
            prepareStatement();
            final String readName = "SELECT intervall FROM Erinnerung;";
            rs = stmt.executeQuery(readName);
            while (rs.next()) {
                intervall = rs.getInt("intervall");
            }
            result = intervall;
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Date getDate() {
        final ResultSet rs;
        Date date = null;
        Date result = null;
        try {
            prepareStatement();
            final String readName = "SELECT termin FROM PATIENT;";
            rs = stmt.executeQuery(readName);
            while (rs.next()) {
                date = rs.getDate("termin");
                termDate.add(date);
            }
            result = date;
            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

    public String getTerm() {
        List<String> data = new ArrayList<String>(selectFromPatient());
        int intervall = getIntervall();
        @SuppressWarnings("unused")
        Date termin = getDate();
        String result = "Kein Termin in" + intervall + " Tagen!";
        int termZaehler = 1;

        if (!isTermStarted) {
            for (int i = 0; i < termDate.size(); i++) {
                LocalDate datum = LocalDate.parse(termDate.get(i).toString());
                LocalDate now = LocalDate.now();
                long test = ChronoUnit.DAYS.between(datum, now);
                try {
                    if (test == intervall) {
                        if (data.get(i).contains(termDate.get(i).toString())) {
                            if (termZaehler == 1) {
                                result = "";
                            }
                            result += "Termin " + termZaehler + " :\n" + data.get(i) + "\n\n";
                            ++termZaehler;
                        }
                    }
                    isTermStarted = true;
                    TerminPop pop = new TerminPop();
                    pop.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public String setTerm(String result) {
        return result;
    }

    private List<String> selectFromPatient() {
        List<String> result = new ArrayList<String>();
        try {
            prepareStatement();
            final String getPatient = "Select Name, Region, Art, Termin, Farbe, Status From Patient";
            final ResultSet rs = stmt.executeQuery(getPatient);
            while (rs.next()) {
                String data = rs.getString("name") + " / " + rs.getString("Region") + " / " + rs.getString("Art")
                        + " / " + rs.getDate("Termin") + " / " + rs.getString("Farbe") + " / " + rs.getString("Status");
                result.add(data);
            }
        } catch (final SQLException e) {
            createTablePatient();
        }
        return result;
    }

    public void deletePatient(String delete, String select) {
        try {
            prepareStatement();
            final String sql = "CREATE TABLE HISTORY_PATIENT" + "(" + "id int identity primary key,"
                    + "Name varchar(255)," + "Region varchar(255)," + "Art varchar(255)," + "Termin date,"
                    + "Farbe varchar(255)," + "Status varchar(255)," + "); INSERT INTO HISTORY_PATIENT("
                    + "name, region, art, termin, farbe, status)select "
                    + "name, region, art, termin, farbe, status from patient where " + select
                    + ";DELETE FROM PATIENT where " + delete;
            stmt.execute(sql);
            conn.close();
        } catch (final SQLException e) {
            try {
                prepareStatement();
                final String sql = "INSERT INTO HISTORY_PATIENT(" + "name, region, art, termin, farbe, status)select "
                        + "name, region, art, termin, farbe, status from patient where " + select
                        + ";DELETE FROM PATIENT where " + delete;
                stmt.execute(sql);
                conn.close();
            } catch (SQLException e1) {
                try {
                    prepareStatement();
                    final String sql = "INSERT INTO HISTORY_PATIENT("
                            + "name, region, art, termin, farbe, status)select "
                            + "name, region, art, termin, farbe, status from patient" + select + ";DELETE FROM PATIENT"
                            + delete;
                    stmt.execute(sql);
                    conn.close();
                } catch (SQLException e2) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public HashMap<String, String> fillPatientSearchGui(String searchString) {
        ResultSet patient;
        String name = "";
        String region = "";
        String art = "";
        String term = "";
        String color = "";
        String status = "";
        HashMap<String, String> result = new HashMap<String, String>();

        try {
            prepareStatement();
            final String getName = "SELECT name FROM PATIENT where name = '" + searchString + "';";
            final String getRegion = "SELECT region FROM PATIENT where name = '" + searchString + "';";
            final String getArt = "SELECT art FROM PATIENT where name = '" + searchString + "';";
            final String getTerm = "SELECT termin FROM PATIENT where name = '" + searchString + "';";
            final String getColor = "SELECT farbe FROM PATIENT where name = '" + searchString + "';";
            final String getStatus = "SELECT status FROM PATIENT where name = '" + searchString + "';";

            patient = stmt.executeQuery(getName);
            while (patient.next()) {
                name = patient.getString("name");
                result.put("name", name);
            }

            patient = stmt.executeQuery(getRegion);
            while (patient.next()) {
                region = patient.getString("region");
                result.put("region", region);
            }

            patient = stmt.executeQuery(getArt);
            while (patient.next()) {
                art = patient.getString("art");
                result.put("art", art);
            }

            patient = stmt.executeQuery(getTerm);
            while (patient.next()) {
                term = patient.getDate("termin").toString();
                result.put("term", term);
            }

            patient = stmt.executeQuery(getColor);
            while (patient.next()) {
                color = patient.getString("farbe");
                result.put("color", color);
            }

            patient = stmt.executeQuery(getStatus);
            while (patient.next()) {
                status = patient.getString("status");
                result.put("status", status);
            }

            conn.close();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

}
